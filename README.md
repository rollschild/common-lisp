# Practical Common Lisp

- Anything in parentheses is a **list**
- Lisp evaluates lists by:
  - treating the first element as the name of a function
  - treating rest of the elements as expressions to be evaluated to yield the
    arguments to the function
- self-evaluating objects
- **side effect**
- to call the function: `(func-name)`
- `(defun name varlist &rest body)`
-
